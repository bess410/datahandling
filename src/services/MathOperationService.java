package services;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class MathOperationService {
    public BigDecimal getCircelSquare(BigDecimal radius, int scale) {
        radius = radius.pow(2).multiply(new BigDecimal(Math.PI));
        radius = radius.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
        return radius;
    }

    public boolean isThirdNumberSumOfTwoOthers(BigDecimal one, BigDecimal two, BigDecimal three) {
        return one.add(two).compareTo(three) == 0;
    }

    public BigDecimal getFromStringLocale(String number, Locale locale) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(locale);
        char separator = dfs.getDecimalSeparator();
        number = number.replace(separator, '.');
        return new BigDecimal(number);
    }

    public double getMaxOfThreeNumbers(double one, double two, double three) {
        return Math.max(one, Math.max(two, three));
    }

    public double getMinOfThreeNumbers(double one, double two, double three) {
        return Math.min(one, Math.min(two, three));
    }
}
