package services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.Period;
import java.util.Locale;

public class DateOperationService {
    public long getAgeInChronoUnits(LocalDateTime birthday, ChronoUnit unit) {
        return unit.between(birthday, LocalDateTime.now());
    }

    public String getAge(LocalDateTime birthday) {
        LocalDateTime now = LocalDateTime.now();
        Period period = Period.between(birthday.toLocalDate(), now.toLocalDate());
        now = now.minus(period);
        long hours = ChronoUnit.HOURS.between(birthday, now);
        now = now.minusHours(hours);
        long minutes = ChronoUnit.MINUTES.between(birthday, now);
        now = now.minusMinutes(minutes);
        long seconds = ChronoUnit.SECONDS.between(birthday, now);
        String result = String.format("You are now %d years, %d months, %d days, %d hours, " +
                        "%d minutes and %d seconds", period.getYears(), period.getMonths(), period.getDays(),
                hours, minutes, seconds);
        return result;
    }

    public long getDaysBetweenDates(String date1, String date2, String pattern) {
        LocalDate firstDate = getDateFromString(date1, pattern);
        LocalDate secondDate = getDateFromString(date2, pattern);

        return Math.abs(ChronoUnit.DAYS.between(secondDate, firstDate));
    }

    private LocalDate getDateFromString(String date, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(date, formatter);
    }

    public String convertToOtherPattern(String from, String to, String input, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(from).withLocale(locale);
        LocalDate date = LocalDate.parse(input, formatter);
        formatter = DateTimeFormatter.ofPattern(to).withLocale(locale);
        return date.format(formatter);
    }
}
