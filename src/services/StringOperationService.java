package services;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringOperationService {
    public String removePunctuationMarks(String string) {
        return string.replaceAll("[\\,\\.\\!\\?\\:\\; ]+", "\n");
    }

    public String getFirstMinWord(String string) {
        String[] arrStrings = string.split("[\\,\\.\\!\\?\\:\\; ]+");
        int minLength = string.length();
        String minWord = "";
        for (String str : arrStrings) {
            if (str.length() < minLength) {
                minLength = str.length();
                minWord = str;
            }
        }
        return minWord;
    }

    public String getFirstMaxWord(String string) {
        String[] arrStrings = string.split("[\\,\\.\\!\\?\\:\\; ]+");
        int maxLength = 0;
        String maxWord = "";
        for (String str : arrStrings) {
            if (str.length() > maxLength) {
                maxLength = str.length();
                maxWord = str;
            }
        }
        return maxWord;
    }

    public String getWithoutIzhevskPrefix(String string) {
        return string.replaceAll("^[\\+]?[78][ ]?[\\(]?3412[\\)]?[ ]?", "");
    }

    public String getCompletedTemplate(String template, Map<String, String> map) {
        Pattern pattern = Pattern.compile("\\$[\\w]+");
        Matcher matcher = pattern.matcher(template);
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(buffer, map.get(matcher.group().substring(1)));
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }
}
