package stringsAndRegex;

import services.StringOperationService;

/*1.1. Дана строка, содержащая "обычный" текст, т.е. слова, знаки препинания, переносы строк и т.п.,
 например, "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?".
Необходимо получить строку, в которой содержатся только слова из исходной строки,
разделенные знаком переноса строки, в нижнем регистре
*/
public class Exercise1_1 {
    public static void main(String[] args) {
        // Можно было конечно регулярку написать, чтобы она слова только вытаскивала, но если не
        // важно на каком языке, то мне кажется лучше знаки препинания выдергивать
        String string = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
        StringOperationService service = new StringOperationService();
        System.out.println(service.removePunctuationMarks(string).toLowerCase());
    }
}
