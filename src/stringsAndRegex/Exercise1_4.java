package stringsAndRegex;

import services.StringOperationService;

import java.util.HashMap;
import java.util.Map;

/*Дана строка-шаблон, содержащая заготовку письма. Например,
"Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета скопилась сумма,
превышающая стоимость $числоМесяцев месяцев пользования нашими услугами. Деньги продолжают поступать.
Вероятно, вы неправильно настроили автоплатеж. С уважением, $пользовательФИО $должностьПользователя"
Также дана одна пара строк. templateKey и templateValue. Необходимо в строке заменить все placeholders
(строки $имяШаблона, т.е. '$' + templateKey) на значения из templateValue
*/
public class Exercise1_4 {
    public static void main(String[] args) {
        String template = "Уважаемый, $userName, извещаем вас о том, что на вашем счете\n" +
                "$numberAccount скопилась сумма, превышающая стоимость " +
                "$numberMonth месяцев пользования нашими услугами. Деньги продолжают поступать.\n" +
                "Вероятно, вы неправильно настроили автоплатеж.\n" +
                "С уважением, $supportUserName $supportUserPosition.";
        Map<String, String> map = new HashMap<>();
        map.put("userName", "Вася Пупкин");
        map.put("numberAccount", "132166");
        map.put("numberMonth", "6");
        map.put("supportUserName", "Путин В. В.");
        map.put("supportUserPosition", "Директор Мира");

        StringOperationService service = new StringOperationService();
        System.out.println(service.getCompletedTemplate(template, map));
    }
}
