package stringsAndRegex;

// В предыдущей задаче найти самое короткое слово, самое длинное слово

import services.StringOperationService;

public class Exercise1_2 {
    public static void main(String[] args) {
        String string = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
        StringOperationService service = new StringOperationService();
        System.out.println("The min length word is " + service.getFirstMinWord(string));
        System.out.println("The max length word is " + service.getFirstMaxWord(string));
    }
}
