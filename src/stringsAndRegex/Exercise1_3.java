package stringsAndRegex;

import services.StringOperationService;

/*Дана строка, содержащая в себе, помимо прочего, номера телефонов.
Необходимо удалить из этой строки префиксы локальных номеров, соответствующих Ижевску.
Например, из "+7 (3412) 517-647" получить "517-647";
"8 (3412) 4997-12" > "4997-12"; "+7 3412 90-41-90" > "90-41-90"
*/
public class Exercise1_3 {
    public static void main(String[] args) {
        String[] telNumbers = new String[]{"+7 (3412) 517-647", "8 (3412) 4997-12", "+7 3412 90-41-90", "7341245-26-90"};
        StringOperationService service = new StringOperationService();
        for (String str : telNumbers) {
            System.out.println(str + " => " + service.getWithoutIzhevskPrefix(str));
        }
    }
}
