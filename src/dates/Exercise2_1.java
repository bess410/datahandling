package dates;

import services.DateOperationService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/*Определить ваш возраст с момента рождения (можно нафантазировать, если нет точных данных =) )
на момент запуска программы. Возраст в секундах, минутах, часах, днях, месяцах и годах
*/
public class Exercise2_1 {
    public static void main(String[] args) {
        LocalDateTime birthday = LocalDateTime.of(1982, 12, 16, 0, 0, 0);
        DateOperationService service = new DateOperationService();
        System.out.println("Your age in years is " + service.getAgeInChronoUnits(birthday, ChronoUnit.YEARS));
        System.out.println("Your age in months is " + service.getAgeInChronoUnits(birthday, ChronoUnit.MONTHS));
        System.out.println("Your age in days is " + service.getAgeInChronoUnits(birthday, ChronoUnit.DAYS));
        System.out.println("Your age in hours is " + service.getAgeInChronoUnits(birthday, ChronoUnit.HOURS));
        System.out.println("Your age in minutes is " + service.getAgeInChronoUnits(birthday, ChronoUnit.MINUTES));
        System.out.println("Your age in seconds is " + service.getAgeInChronoUnits(birthday, ChronoUnit.SECONDS));
        System.out.println(service.getAge(birthday));
    }
}
