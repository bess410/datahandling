package dates;

import services.DateOperationService;

/*Даны две даты в виде "25.07.1921", т.е. "день месяца 2 разряда, месяц 2 разряда, год 4 разряда".
 Найти разницу в днях между ними. Число должно быть всегда положительным
*/
public class Exercise2_2 {
    public static void main(String[] args) {
        String pattern = "dd.MM.yyyy";
        String date1 = "25.07.1921";
        String date2 = "16.12.2017";
        DateOperationService service = new DateOperationService();
        System.out.println("The difference between " + date1 + " and " + date2 +
                " is " + service.getDaysBetweenDates(date1, date2, pattern)
                + " days.");
    }
}
