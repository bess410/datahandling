package dates;

import services.DateOperationService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/*Дана строка вида "28 Февраль 2015" или “28 February 2015” (*"28 февраля 2015").
Необходимо сконвертировать ее в вид “28/фев/15” или "28/Feb/15".
Локаль учитывать не нужно (* нужно). Использовать SimpleDateFormat или DateTimeFormatter
*/
public class Exercise2_3 {
    public static void main(String[] args) {
        String input1 = "25 февраля 1952";
        String input2 = "17 January 1954";
        String from = "dd MMMM yyyy";
        String to = "dd/MMM/yy";
        DateOperationService service = new DateOperationService();
        System.out.println(input1 + " converted to " + service.convertToOtherPattern(from, to, input1, Locale.getDefault()));
        System.out.println(input2 + " converted to " + service.convertToOtherPattern(from, to, input2, Locale.ENGLISH));
    }
}
