package codingbat.string3;

/*Given a string, look for a mirror image (backwards) string at both the beginning and end of the given string.
In other words, zero or more characters at the very begining of the given string, and at the very end
 of the string in reverse order (possibly overlapping). For example, the string "abXYZba" has the mirror end "ab".*/
public class MirrorEnds {
    public static void main(String[] args) {
        String string1 = "abXYZba";
        System.out.println("The mirrorEnd of " + string1 + " is " + mirrorEnds(string1));
        String string2 = "abca";
        System.out.println("The mirrorEnd of " + string2 + " is " + mirrorEnds(string2));
        String string3 = "aba";
        System.out.println("The mirrorEnd of " + string3 + " is " + mirrorEnds(string3));
    }

    public static String mirrorEnds(String string) {
        int firstIndex = string.length() % 2 == 0 ? string.length() / 2 : string.length() / 2 + 1;
        int secondIndex = string.length() / 2;
        String firstString = string.substring(0, firstIndex);
        String secondString = string.substring(secondIndex);
        secondString = new StringBuilder(secondString).reverse().toString();
        if (firstString.equals(secondString)) {
            return string;
        }
        for (int i = 0; i < firstString.length(); i++) {
            if (firstString.charAt(i) != secondString.charAt(i)) {
                return firstString.substring(0, i);
            }
        }
        return "";
    }
}
