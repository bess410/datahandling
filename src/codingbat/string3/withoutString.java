package codingbat.string3;

/*Given two strings, base and remove, return a version of the base string where all instances of the remove
string have been removed (not case sensitive). You may assume that the remove string is length 1 or more.
Remove only non-overlapping instances, so with "xxx" removing "xx" leaves "x".
*/
public class withoutString {
    public static void main(String[] args) {
        String hello = "Hello there";
        System.out.println(hello + " - llo = " + withoutString(hello, "llo"));
        System.out.println(hello + " - e = " + withoutString(hello, "e"));
        System.out.println(hello + " - x = " + withoutString(hello, "x"));
    }

    public static String withoutString(String base, String remove) {
        return base.replaceAll("(?i)" + remove.toLowerCase(), "");
    }
}
