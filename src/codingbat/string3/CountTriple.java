package codingbat.string3;

/*We'll say that a "triple" in a string is a char appearing three times in a row.
Return the number of triples in the given string. The triples may overlap.*/
public class CountTriple {
    public static void main(String[] args) {
        String string1 = "abcXXXabc";
        System.out.println("CountTriple of " + string1 + " is " + countTriple(string1));
        String string2 = "xxxabyyyycd";
        System.out.println("CountTriple of " + string2 + " is " + countTriple(string2));
        String string3 = "a";
        System.out.println("CountTriple of " + string3 + " is " + countTriple(string3));
    }

    public static int countTriple(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 2; i++) {
            char ch = str.charAt(i);
            if (ch == str.charAt(i + 1) && (ch == str.charAt(i + 2))) {
                count++;
            }
        }

        return count;
    }
}
