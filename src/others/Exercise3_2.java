package others;

import services.MathOperationService;

import java.math.BigDecimal;
import java.util.Locale;

/*Даны три числа, например, 0.1, 0.15 и 0.25. Числа даны в виде строки. Необходимо ответить,
является ли третье число суммой двух первых.
* Учесть локаль пользователя и разделитель целой-дробной частей в данных строках
*/
public class Exercise3_2 {
    public static void main(String[] args) {
        String one = "0.1";
        String two = "0.15";
        String three = "0.25";
        Locale locale = Locale.ENGLISH;
        MathOperationService service = new MathOperationService();
        BigDecimal first = service.getFromStringLocale(one, locale);
        BigDecimal second = service.getFromStringLocale(two, locale);
        BigDecimal third = service.getFromStringLocale(three, locale);
        System.out.println("Is " + three + " sum of " + one + " and " + two + ": " + service.isThirdNumberSumOfTwoOthers(first, second, third));
        one = "0,1";
        two = "0,13";
        three = "0,23";
        locale = Locale.CANADA_FRENCH;
        first = service.getFromStringLocale(one, locale);
        second = service.getFromStringLocale(two, locale);
        third = service.getFromStringLocale(three, locale);
        System.out.println("Is " + three + " sum of " + one + " and " + two + ": " + service.isThirdNumberSumOfTwoOthers(first, second, third));
    }
}
