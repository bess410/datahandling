package others;

import services.MathOperationService;

/**
 * Даны три числа. Нужно найти минимум и максимум не используя условный и тернарный операторы
 */
public class Exercise3_3 {
    public static void main(String[] args) {
        MathOperationService service = new MathOperationService();
        double one = 0.2134;
        double two = 0.2344;
        double three = 0.2349;
        System.out.printf("The max of %f, %f and %f is %f", one, two, three, service.getMaxOfThreeNumbers(one, two, three));
        System.out.println();
        System.out.printf("The min of %f, %f and %f is %f", one, two, three, service.getMinOfThreeNumbers(one, two, three));
    }
}
