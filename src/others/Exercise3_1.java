package others;

import services.MathOperationService;

import java.math.BigDecimal;

/*Необходимо посчитать площадь круга с указанным радиусом с точностью 50 знаков после запятой
*/
public class Exercise3_1 {
    public static void main(String[] args) {
        BigDecimal radius = new BigDecimal("19879.9878979878979879879879879878987897987");
        MathOperationService service = new MathOperationService();
        System.out.println("Square of circle with radius " + radius + " is " + service.getCircelSquare(radius, 50));
    }
}
